﻿// Main.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Console.h"
#include "Company.h"
#include "CompanyException.h"

int wmain(int argc, wchar_t **argv)
{
	// Установить Unicode для консоли
	SetUnicodeConsole();

	// Прочитать имена файлов из аргументов
	std::vector<std::experimental::filesystem::path> filePaths;
	for (auto i = 1; i < argc; ++i)
		filePaths.push_back(argv[i]);

	// Создать предприятие
	Company company(filePaths);

	// Запустить цикл обработки команд для предприятия
	while (true)
	{
		try
		{
			std::wcout << ">> ";
			std::wstring line;
			std::getline(std::wcin, line);
			company.ExecuteCommandLine(line);
			std::wcout << std::endl;
		}
		catch (const CompanyException& e)
		{
			std::wcout << e.what() << std::endl << std::endl;
		}
	}
	return 0;
}