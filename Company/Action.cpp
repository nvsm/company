﻿#include "stdafx.h"
#include "Action.h"
#include "CompanyException.h"

ActionMap::ActionMap()
{
	map[L"уйти в отпуск"] = ActionType::Vacation;
	map[L"убрать рабочее место"] = ActionType::Clean;
	map[L"программировать"] = ActionType::Develop;
	map[L"проектировать"] = ActionType::Design;
	map[L"переводить тексты"] = ActionType::Translate;
	map[L"тестировать"] = ActionType::Test;
	map[L"составлять тестовые планы"] = ActionType::MakeTestPlans;
	map[L"начислять зарплату"] = ActionType::PaySalary;
	map[L"составить квартальный отчет"] = ActionType::MakeQuarterlyReport;
}

// Получить тип действия по имени
ActionType ActionMap::StringToType(const std::wstring& str) const
{
	auto type = map.find(str);
	if (type == map.end())
		throw UnknownActionException(str);
	return type->second;
}