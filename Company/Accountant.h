﻿#pragma once
#include "Employee.h"

// Бухгалтер
class Accountant : public Employee
{
public:
	Accountant(const std::wstring& name) : Employee(name) {}

	// Выполнить действие
	virtual bool Execute(ActionType type);
};