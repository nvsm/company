﻿#pragma once
#include "Employee.h"

// Специалист тестирования
class TestingSpecialist : public Employee
{
public:
	TestingSpecialist(const std::wstring& name) : Employee(name) {}

	// Выполнить действие
	virtual bool Execute(ActionType type);
};
