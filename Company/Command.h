﻿#pragma once

// Команда
class Command
{
public:
	// Парсить строку команды
	void Parse(const std::wstring& line);

	// Наименование
	const auto& Name() const { return name; }
	// Параметры
	const auto& Params() const { return params; }

private:
	// Наименование
	std::wstring name;
	// Параметры
	std::vector<std::wstring> params;
};
