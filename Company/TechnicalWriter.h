﻿#pragma once
#include "Employee.h"

// Технический писатель
class TechnicalWriter : public Employee
{
public:
	TechnicalWriter(const std::wstring& name) : Employee(name) {}

	// Выполнить действие
	virtual bool Execute(ActionType type);
};