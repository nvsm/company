﻿#include "stdafx.h"
#include "TestingSpecialist.h"

// Выполнить действие
bool TestingSpecialist::Execute(ActionType type)
{
	if (Employee::Execute(type))
		return true;
	switch (type)
	{
	case ActionType::Test:
	case ActionType::MakeTestPlans:
		return true;
	}
	return false;
}