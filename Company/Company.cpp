﻿#include "stdafx.h"
#include "Company.h"
#include "Command.h"
#include "Department.h"
#include "CompanyException.h"
#include <codecvt>

// Выполнить строку команды
void Company::ExecuteCommandLine(const std::wstring& line)
{
	Command command;
	command.Parse(line);
	if (command.Name() == L"загрузить")
		LoadCommand(command.Params());
	else if (command.Name() == L"выполнить")
		ExecuteCommand(command.Params());
	else if (command.Name().empty())
		throw NoCommandException();
	else
		throw UnknownCommandException(command.Name());
}

// Команда "загрузить"
void Company::LoadCommand(const std::vector<std::wstring>& params)
{
	if (params.empty())
		throw NoDepartmentException();
	std::wstring name = params[0];
	auto stem = [&](const auto& p) { return name == p.stem(); };
	auto filePath = std::find_if(filePaths.begin(), filePaths.end(), stem);
	if (filePath == filePaths.end())
		throw DepartmentAbsentException(name);
	auto department = std::make_unique<Department>(name);
	std::wifstream input(*filePath);
	if (!input.is_open())
		throw OpenFileException(*filePath);
	static auto locale = std::locale(std::locale::empty(),
		new std::codecvt_utf8<wchar_t>);
	input.imbue(locale);
	input >> *department;
	items[name] = std::move(department);
	std::wcout << L"отдел \"" + name + L"\" загружен" << std::endl;
}

// Команда "выполнить"
void Company::ExecuteCommand(const std::vector<std::wstring>& params)
{
	static ActionMap actionMap;
	if (params.empty())
		throw NoActionException();
	auto type = actionMap.StringToType(params[0]);
	ExecuteSequence(type, params.begin() + 1, params.end());
}