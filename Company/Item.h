﻿#pragma once
#include "Action.h"

// Элемент организации
class Item
{
public:
	Item(const std::wstring& name) : name(name) {}
	virtual ~Item() {}

	// Описание
	virtual std::wstring Description() const { return name; };

	// Выполнить действие
	virtual bool Execute(ActionType type) { return false; };
	// Выполнить действие по цепочке
	template <typename It>
	bool ExecuteSequence(ActionType type, It first, It last);

	// Распечатать результат выполнения действия
	void PrintResult(bool result) const;

protected:
	// Наименование
	std::wstring name;
	// Подчиненные
	std::unordered_map<std::wstring, std::unique_ptr<Item>> items;
};

// Выполнить действие по цепочке
template <typename It>
bool Item::ExecuteSequence(ActionType type, It first, It last)
{
	bool result = false;
	if (first != last)
	{
		// Выполнить у следующего элемента по цепочке
		auto item = items.find(*first);
		if (item == items.end())
			throw UnknownNameException(*first);
		result |= item->second->ExecuteSequence(type, ++first, last);
	}
	else
	{
		// Выполнить у всех подчиненных элементов
		for (auto& item : items)
			result |= item.second->ExecuteSequence(type, first, last);
	}
	// Выполнить у текущего элемента
	result |= Execute(type);
	PrintResult(result);
	return result;
}