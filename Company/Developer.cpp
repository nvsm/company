﻿#include "stdafx.h"
#include "Developer.h"

// Выполнить действие
bool Developer::Execute(ActionType type) {
	if (Employee::Execute(type))
		return true;
	switch (type)
	{
	case ActionType::Develop:
	case ActionType::Design:
		return true;
	}
	return false;
}