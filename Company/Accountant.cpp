﻿#include "stdafx.h"
#include "Accountant.h"

// Выполнить действие
bool Accountant::Execute(ActionType type)
{
	if (Employee::Execute(type))
		return true;
	switch (type)
	{
	case ActionType::PaySalary:
	case ActionType::MakeQuarterlyReport:
		return true;
	}
	return false;
}