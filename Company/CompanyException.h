﻿#pragma once

// Пользовательское исключение
class CompanyException
{
public:
	CompanyException(const std::wstring& description)
		: description(description.c_str()) {}
	virtual ~CompanyException() {}
	const std::wstring& what() const { return description; }

protected:
	std::wstring description;
};

// Неизвестное действие
class UnknownActionException : public CompanyException
{
public:
	UnknownActionException(const std::wstring& type)
		: CompanyException(L"неизвестное действие: \"" + type + L"\"") {}
};

// Не указано действие
class NoActionException : public CompanyException
{
public:
	NoActionException()
		: CompanyException(L"не указано действие") {}
};

// Неизвестное имя
class UnknownNameException : public CompanyException
{
public:
	UnknownNameException(const std::wstring& name)
		: CompanyException(L"неизвестное имя: \"" + name + L"\"") {}
};

// Неизвестный тип сотрудника
class UnknownEmployeeTypeException : public CompanyException
{
public:
	UnknownEmployeeTypeException(const std::wstring& type)
		: CompanyException(L"неизвестный тип сотрудника: \"" + type + L"\"") {}
};

// Не указана команда
class NoCommandException : public CompanyException
{
public:
	NoCommandException()
		: CompanyException(L"не указана команда") {}
};


// Неизвестная команда
class UnknownCommandException : public CompanyException
{
public:
	UnknownCommandException(const std::wstring& command)
		: CompanyException(L"неизвестная команда: \"" + command + L"\"") {}
};

// Не указан отдел
class NoDepartmentException : public CompanyException
{
public:
	NoDepartmentException()
		: CompanyException(L"не указан отдел") {}
};

// Отдел отсутствует
class DepartmentAbsentException : public CompanyException
{
public:
	DepartmentAbsentException(const std::wstring& department)
		: CompanyException(L"отдел \"" + department + L"\" отсутствует") {}
};

// Ошибка открытия файла
class OpenFileException : public CompanyException
{
public:
	OpenFileException(const std::wstring& path)
		: CompanyException(L"ошибка открытия файла \"" + path + L"\"") {}
};

// Некорректный файл департамента
class IncorrectDepartmentFileException : public CompanyException
{
public:
	IncorrectDepartmentFileException(const std::wstring& reason)
		: CompanyException(L"некорректный файл департамента: " + reason) {}
};

// Некорректные колонки в файле департамента
class IncorrentColumnsInDepartmentFileException
	: public IncorrectDepartmentFileException
{
public:
	IncorrentColumnsInDepartmentFileException(const std::wstring& expected)
		: IncorrectDepartmentFileException(
			L"ожидаются колонки \"" + expected + L"\"") {}
};

// В строке данных файла департамента отсутствует разделитель
class NoDataRowDelimeterInDepartmentFileException
	: public IncorrectDepartmentFileException
{
public:
	NoDataRowDelimeterInDepartmentFileException()
		: IncorrectDepartmentFileException(
			L"в строке данных отсутствует разделитель \";\"") {}
};