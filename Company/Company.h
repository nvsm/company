﻿#pragma once
#include "Item.h"

// Предприятие
class Company : public Item
{
public:
	Company(const std::vector<std::experimental::filesystem::path>& filePaths)
		: Item(L"предприятие"), filePaths(filePaths) {}

	// Выполнить строку команды
	void ExecuteCommandLine(const std::wstring& line);

private:
	// Команда "загрузить"
	void LoadCommand(const std::vector<std::wstring>& params);
	// Команда "выполнить"
	void ExecuteCommand(const std::vector<std::wstring>& params);

	// Пути к файлам отделов
	std::vector<std::experimental::filesystem::path> filePaths;
};