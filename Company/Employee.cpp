﻿#include "stdafx.h"
#include "Employee.h"

// Описание
std::wstring Employee::Description() const
{
	return L"сотрудник \"" + name + L"\"";
}

// Выполнить действие
bool Employee::Execute(ActionType type)
{
	switch (type)
	{
	case ActionType::Vacation:
	case ActionType::Clean:
		return true;
	}
	return false;
}