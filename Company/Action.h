﻿#pragma once

// Типы действий
enum class ActionType
{
	Vacation,
	Clean,
	Develop,
	Design,
	Translate,
	Test,
	MakeTestPlans,
	PaySalary,
	MakeQuarterlyReport
};

// Карта, отражающая строки в типы действий
class ActionMap
{
public:
	ActionMap();

	// Получить тип действия по имени
	ActionType StringToType(const std::wstring& str) const;

private:
	// Карта <строка, тип действия>
	std::unordered_map<std::wstring, ActionType> map;
};