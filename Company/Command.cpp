﻿#include "stdafx.h"
#include "Command.h"
#include "CompanyException.h"

// Парсить строку команды
void Command::Parse(const std::wstring& line)
{
	name.clear();
	params.clear();
	std::vector<std::wstring> tokens;
	auto position = line.find_first_not_of(L" ");
	if (position == std::wstring::npos)
		return;
	auto next = position;
	while (true)
	{
		if (line[position] == L'\"')
			next = line.find(L'\"', ++position);
		else
			next = line.find(L' ', position);
		if (next == std::wstring::npos)
		{
			tokens.push_back(line.substr(position));
			break;
		}
		else
		{
			tokens.push_back(line.substr(position, next - position));
			position = line.find_first_not_of(L" ", ++next);
			if (position == std::wstring::npos)
				break;
		}
	}
	if (!tokens.empty())
	{
		name = tokens[0];
		params.assign(tokens.begin() + 1, tokens.end());
	}
}
