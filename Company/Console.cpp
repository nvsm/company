﻿#include "stdafx.h"
#include "Console.h"
#include <fcntl.h>
#include <io.h>

// Установить Unicode для консоли
void SetUnicodeConsole()
{
	_setmode(_fileno(stdout), _O_U16TEXT);
	_setmode(_fileno(stdin), _O_U16TEXT);
	_setmode(_fileno(stderr), _O_U16TEXT);
}
