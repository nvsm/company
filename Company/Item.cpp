﻿#include "stdafx.h"
#include "Item.h"

// Распечатать результат выполнения действия
void Item::PrintResult(bool result) const
{
	std::wcout << Description() << L": ";
	if (result)
		std::wcout << L"сделано";
	else
		std::wcout << L"не сделано";
	std::wcout << std::endl;
}