﻿#pragma once
#include "Employee.h"

// Разработчик
class Developer : public Employee
{
public:
	Developer(const std::wstring& name) : Employee(name) {}

	// Выполнить действие
	virtual bool Execute(ActionType type);
};