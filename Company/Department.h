﻿#pragma once
#include "Item.h"

// Отдел
class Department : public Item
{
public:
	Department(const std::wstring& name) : Item(name) {}

	// Описание
	virtual std::wstring Description() const;

protected:
	// Создать сотрудника отдела
	auto createItem(const std::wstring& name, const std::wstring& type) const;

	// Прочитать данные отдела из потока
	friend std::wistream& operator>>(std::wistream& input, Department& dep);
};