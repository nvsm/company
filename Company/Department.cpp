﻿#include "stdafx.h"
#include "Department.h"
#include "CompanyException.h"
#include "Developer.h"
#include "TechnicalWriter.h"
#include "TestingSpecialist.h"
#include "Accountant.h"

// Описание
std::wstring Department::Description() const
{
	return L"отдел \"" + name + L"\"";
}

// Создать сотрудника отдела
auto Department::createItem(const std::wstring& name,
	const std::wstring& type) const
{
	if (type == L"Разработчик")
		return std::unique_ptr<Item>(new Developer(name));
	else if (type == L"Технический писатель")
		return std::unique_ptr<Item>(new TechnicalWriter(name));
	else if (type == L"Специалист тестирования")
		return std::unique_ptr<Item>(new TestingSpecialist(name));
	else if (type == L"Бухгалтер")
		return std::unique_ptr<Item>(new Accountant(name));
	throw UnknownEmployeeTypeException(type);
	return std::unique_ptr<Item>();
}

// Прочитать данные отдела из потока
std::wistream& operator>>(std::wistream& input, Department& department)
{
	std::wstring line;
	std::getline(input, line);
	std::wstring columns = L"Фамилия;Специальность";
	if (line != columns)
		throw IncorrentColumnsInDepartmentFileException(columns);
	while (std::getline(input, line))
	{
		auto delim = line.find(L";");
		if (delim == std::wstring::npos)
			throw NoDataRowDelimeterInDepartmentFileException();
		std::wstring name = line.substr(0, delim);
		std::wstring type = line.substr(delim + 1);
		department.items[name] = department.createItem(name, type);
	}
	return input;
}