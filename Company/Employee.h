﻿#pragma once
#include "Item.h"

class Employee : public Item
{
public:
	Employee(const std::wstring& name) : Item(name) {}

	// Описание
	virtual std::wstring Description() const;

	// Выполнить действие
	virtual bool Execute(ActionType type);
};