﻿#pragma once

// Перенаправление стандартного потока вывода
class CoutRedirect
{
public:
	CoutRedirect(std::wstreambuf* buffer)
		: old(std::wcout.rdbuf(buffer)) {}
	CoutRedirect()
	{
		std::wcout.rdbuf(old);
	}

private:
	std::wstreambuf* old;
};