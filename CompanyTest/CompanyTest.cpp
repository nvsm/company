﻿#include "stdafx.h"
#include "CppUnitTest.h"
#include "Company.h"
#include "CompanyException.h"
#include "DataFile.h"
#include "CoutRedirect.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CompanyTest
{
	// Тесты обработки предприятием командной строки
	TEST_CLASS(CompanyExecuteCommandLineTest)
	{
	private:
		auto FilePaths()
		{
			std::vector<std::experimental::filesystem::path> filePaths;
			filePaths.push_back(DepartmentFile(L"Department1").Path());
			filePaths.push_back(DepartmentFile(L"Department2").Path());
			return filePaths;
		}

		void AssertWorkFlow(const std::wstring& inputName,
			const std::wstring& outputName)
		{
			InputFile input(inputName);
			std::wstringstream actual;
			CoutRedirect redirect(actual.rdbuf());
			std::wstring line;
			while (std::getline(input.Stream(), line))
			{
				try
				{
					company.ExecuteCommandLine(line);
				}
				catch (const CompanyException& e)
				{
					std::wcout << e.what() << std::endl;
				}
			}
			OutputFile output(outputName);
			std::wstringstream expected;
			expected << output.Stream().rdbuf();
			Assert::AreEqual(expected.str(), actual.str());
		}

	public:
		CompanyExecuteCommandLineTest() : company(FilePaths()) {}

		// Не указана команда
		TEST_METHOD(NoCommand)
		{
			auto functor = [this]() { company.ExecuteCommandLine(L""); };
			Assert::ExpectException<NoCommandException>(functor);
		}

		// Неизвестная команда
		TEST_METHOD(UnknownCommand)
		{
			auto functor = [this]() { company.ExecuteCommandLine(L"н"); };
			Assert::ExpectException<UnknownCommandException>(functor);
		}

		// Не указан отдел
		TEST_METHOD(NoDepartment)
		{
			auto functor = [this]()
			{
				company.ExecuteCommandLine(L"загрузить");
			};
			Assert::ExpectException<NoDepartmentException>(functor);
		}

		// Отдел отсутствует
		TEST_METHOD(DepartmentAbsent)
		{
			auto functor = [this]()
			{
				company.ExecuteCommandLine(L"загрузить Department3");
			};
			Assert::ExpectException<DepartmentAbsentException>(functor);
		}

		// Не указано действие
		TEST_METHOD(NoActionType)
		{
			auto functor = [this]()
			{
				company.ExecuteCommandLine(L"выполнить");
			};
			Assert::ExpectException<NoActionException>(functor);
		}

		// Неизвестное действие
		TEST_METHOD(UnknownActionType)
		{
			auto functor = [this]()
			{
				company.ExecuteCommandLine(L"выполнить н");
			};
			Assert::ExpectException<UnknownActionException>(functor);
		}

		// Рабочий процесс 1
		TEST_METHOD(WorkFlow1)
		{
			AssertWorkFlow(L"Input1", L"Output1");
		}

		// Рабочий процесс 2
		TEST_METHOD(WorkFlow2)
		{
			AssertWorkFlow(L"Input2", L"Output2");
		}

	private:
		Company company;
	};
}