﻿#include "stdafx.h"
#include "CppUnitTest.h"
#include "Accountant.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CompanyTest
{
	// Тесты бухгалтера
	TEST_CLASS(AccountantTest)
	{
	public:
		AccountantTest() : accountant(L"Тест") {}

		// Начислять зарплату
		TEST_METHOD(PaySalary)
		{
			Assert::IsTrue(accountant.Execute(ActionType::PaySalary));
		}

		// Составить квартальный отчет
		TEST_METHOD(MakeQuarterlyReport)
		{
			Assert::IsTrue(accountant.Execute(ActionType::MakeQuarterlyReport));
		}

		// Неподдерживаемое действие
		TEST_METHOD(UnsupportedAction)
		{
			Assert::IsFalse(accountant.Execute(ActionType::Develop));
		}

	private:
		// Бухгалтер
		Accountant accountant;
	};
}