﻿#include "stdafx.h"
#include "DataFile.h"
#include <codecvt>
#include <filesystem>

namespace fs = std::experimental::filesystem;

DataFile::DataFile(const std::wstring& path)
{
	this->path = fs::path(__FILE__).remove_filename() / path;
	stream.open(this->path);
	// Файл должен быть в формате UTF-8
	static auto locale = std::locale(std::locale::empty(),
		new std::codecvt_utf8<wchar_t>);
	stream.imbue(locale);
}