#pragma once

#include "CppUnitTest.h"
#include "CoutRedirect.h"

class CoutAssert
{
public:
	template<typename F>
	static void AreEqual(const std::wstringstream& expected, F functor,
		const wchar_t* message = NULL,
		const Microsoft::VisualStudio::CppUnitTestFramework::__LineInfo* pLineInfo = NULL)
	{
		using Microsoft::VisualStudio::CppUnitTestFramework::Assert;
		std::wstringstream actual;
		CoutRedirect redirect(actual.rdbuf());
		functor();
		Assert::AreEqual(expected.str(), actual.str(), message, pLineInfo);
	}
};