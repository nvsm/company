﻿#include "stdafx.h"
#include "CppUnitTest.h"
#include "Command.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CompanyTest
{
	// Тесты команды
	TEST_CLASS(CommandTest)
	{
	public:
		// Наименование
		TEST_METHOD(Name)
		{
			std::wstring name = L"загрузить";
			command.Parse(name);
			Assert::AreEqual(name, command.Name());
		}

		// Наименование с параметрами
		TEST_METHOD(NameWithParams)
		{
			std::wstring name = L"загрузить";
			command.Parse(name + L" департамент");
			Assert::AreEqual(name, command.Name());
		}

		// Параметры
		TEST_METHOD(Params)
		{
			std::wstring params = L"задача департамент";
			command.Parse(L"выполнить " + params);
			Assert::IsTrue(2 == command.Params().size());
			Assert::AreEqual(L"задача", command.Params()[0].c_str());
			Assert::AreEqual(L"департамент", command.Params()[1].c_str());
		}

	private:
		// Команда
		Command command;
	};
}