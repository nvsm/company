﻿#pragma once

// Файл данных
class DataFile
{
public:
	DataFile(const std::wstring& path);
	virtual ~DataFile() {};

	// Поток данных
	auto& Stream() { return stream; }
	// Путь
	const auto& Path() const { return path; }

protected:
	std::wifstream stream;
	std::wstring path;
};

// Файл отдела
class DepartmentFile : public DataFile
{
public:
	DepartmentFile(const std::wstring& departmentName)
		: DataFile(L"Department\\" + departmentName + L".csv") {}
};

// Файл ввода
class InputFile : public DataFile
{
public:
	InputFile(const std::wstring& inputName)
		: DataFile(L"Input\\" + inputName + L".txt") {}
};

// Файл вывода
class OutputFile : public DataFile
{
public:
	OutputFile(const std::wstring& inputName)
		: DataFile(L"Output\\" + inputName + L".txt") {}
};