﻿#include "stdafx.h"
#include "CppUnitTest.h"
#include "DataFile.h"
#include "Department.h"
#include "CompanyException.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CompanyTest
{
	// Тесты описания отдела
	TEST_CLASS(DepartmentDescriptionTest)
	{
	public:
		DepartmentDescriptionTest() : department(L"Тест") {}

		// Описание
		TEST_METHOD(Description)
		{
			Assert::AreEqual(L"отдел \"Тест\"",
				department.Description().c_str());
		}

	private:
		// Отдел
		Department department;
	};

	// Тесты загрузки данных отдела
	TEST_CLASS(DepartmentLoadTest)
	{
	public:
		DepartmentLoadTest() : department(L"Тест") {}

		auto LoadFunctor(const std::wstring& departmentName)
		{
			return [=]()
			{
				DepartmentFile file(departmentName);
				file.Stream() >> department;
			};
		}

		// Загрузка данных
		TEST_METHOD(Load)
		{
			DepartmentFile file(L"Department1");
			file.Stream() >> department;
		}

		// Загрузка неизвестного типа сотрудника
		TEST_METHOD(LoadUnknownEmployeeType)
		{
			auto functor = LoadFunctor(L"UnknownEmployeeType");
			Assert::ExpectException<UnknownEmployeeTypeException>(functor);
		}

		// Загрузка файла с некорректными колонками
		TEST_METHOD(LoadIncorectColumnNames)
		{
			auto functor = LoadFunctor(L"IncorectColumnNames");
			Assert::ExpectException<IncorrentColumnsInDepartmentFileException>(
				functor);
		}

		// Загрузка файла без разделителя в строке данных
		TEST_METHOD(LoadRowWithoutDelimeter)
		{
			auto functor = LoadFunctor(L"RowWithoutDelimeter");
			Assert::ExpectException<NoDataRowDelimeterInDepartmentFileException>(
				functor);
		}

	private:
		// Отдел
		Department department;
	};

	// Тесты выполнения отделом действий
	TEST_CLASS(DepartmentExecuteTest)
	{
	public:
		DepartmentExecuteTest() : department(L"Тест") {}

		TEST_METHOD_INITIALIZE(Init)
		{
			DepartmentFile file(L"Department2");
			file.Stream() >> department;
		}

		// Выполнить поддерживаемое действие
		TEST_METHOD(ExecuteSupportedAction)
		{
			std::vector<std::wstring> sequence;
			Assert::IsTrue(department.ExecuteSequence(ActionType::Develop,
				sequence.begin(), sequence.end()));
		}

		// Выполнить неподдерживаемое действие
		TEST_METHOD(ExecuteUnsupportedAction)
		{
			std::vector<std::wstring> sequence;
			Assert::IsFalse(department.ExecuteSequence(ActionType::Test,
				sequence.begin(), sequence.end()));
		}

		// Выполнить действие с помощью конкретного сотрудника.
		// Сотрудник поддерживает действие
		TEST_METHOD(ExecuteSupportedByEmployee)
		{
			std::vector<std::wstring> sequence = { L"Соколов" };
			Assert::IsTrue(department.ExecuteSequence(ActionType::Develop,
				sequence.begin(), sequence.end()));
		}

		// Выполнить действие с помощью конкретного сотрудника.
		// Сотрудник не поддерживает действие
		TEST_METHOD(ExecuteUnsupportedByEmployee)
		{
			std::vector<std::wstring> sequence = { L"Соколов" };
			Assert::IsFalse(department.ExecuteSequence(ActionType::Test,
				sequence.begin(), sequence.end()));
		}

		// Выполнить цепочку, размером больше одного
		TEST_METHOD(ExecuteBySequenceSizeMoreThanOne)
		{
			auto functor = [this]()
			{
				std::vector<std::wstring> sequence = { L"Соколов", L"Петров" };
				Assert::IsFalse(department.ExecuteSequence(ActionType::Test,
					sequence.begin(), sequence.end()));
			};
			Assert::ExpectException<UnknownNameException>(functor);
		}

	private:
		// Отдел
		Department department;
	};
}