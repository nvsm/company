﻿#include "stdafx.h"
#include "CppUnitTest.h"
#include "Developer.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CompanyTest
{
	// Тесты разработчика
	TEST_CLASS(DeveloperTest)
	{
	public:
		DeveloperTest() : developer(L"Тест") {}

		// Программировать
		TEST_METHOD(Develop)
		{
			Assert::IsTrue(developer.Execute(ActionType::Develop));
		}

		// Проектировать
		TEST_METHOD(Design)
		{
			Assert::IsTrue(developer.Execute(ActionType::Design));
		}

		// Неподдерживаемое действие
		TEST_METHOD(UnsupportedAction)
		{
			Assert::IsFalse(developer.Execute(ActionType::Translate));
		}

	private:
		// Разработчик
		Developer developer;
	};
}