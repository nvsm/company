﻿#include "stdafx.h"
#include "CppUnitTest.h"
#include "Item.h"
#include "CompanyException.h"
#include "CoutAssert.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CompanyTest
{
	// Тесты для элемента организации
	TEST_CLASS(ItemTest)
	{
	public:
		ItemTest() : item(L"Тест") {}

		// Описание
		TEST_METHOD(Description)
		{
			Assert::AreEqual(L"Тест", item.Description().c_str());
		}

		// Выполнить действие
		TEST_METHOD(Execute)
		{
			Assert::IsFalse(item.Execute(ActionType::Develop));
		}

		// Выполнить действие по цепочке
		TEST_METHOD(ExecuteSequence)
		{
			std::wstringstream stream;
			CoutRedirect redirect(stream.rdbuf());
			std::vector<std::wstring> sequence = {};
			Assert::IsFalse(item.ExecuteSequence(ActionType::Develop,
				sequence.begin(), sequence.end()));
		}

		// Неизвестое имя в действие по цепочке
		TEST_METHOD(UnknownNameInSequence)
		{
			auto functor = [this]()
			{
				std::vector<std::wstring> sequence = { L"Отдел" };
				item.ExecuteSequence(ActionType::Test, sequence.begin(), sequence.end());
			};
			Assert::ExpectException<UnknownNameException>(functor);
		}

		// Положительный результат
		TEST_METHOD(TrueResult)
		{
			auto functor = [this]() { item.PrintResult(true); };
			std::wstringstream expected;
			expected << L"Тест: сделано" << std::endl;
			CoutAssert::AreEqual(expected, functor);
		}

		// Отрицательный результат
		TEST_METHOD(FalseResult)
		{
			auto functor = [this]() { item.PrintResult(false); };
			std::wstringstream expected;
			expected << L"Тест: не сделано" << std::endl;
			CoutAssert::AreEqual(expected, functor);
		}

	private:
		Item item;
	};
}