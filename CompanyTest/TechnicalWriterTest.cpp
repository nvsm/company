﻿#include "stdafx.h"
#include "CppUnitTest.h"
#include "TechnicalWriter.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CompanyTest
{
	// Тесты технического писателя
	TEST_CLASS(TechnicalWriterTest)
	{
	public:
		TechnicalWriterTest() : technicalWriter(L"Тест") {}

		// Переводить тексты
		TEST_METHOD(Translate)
		{
			Assert::IsTrue(technicalWriter.Execute(ActionType::Translate));
		}

		// Неподдерживаемое действие
		TEST_METHOD(UnsupportedAction)
		{
			Assert::IsFalse(technicalWriter.Execute(ActionType::Test));
		}

	private:
		// Технический писатель
		TechnicalWriter technicalWriter;
	};
}