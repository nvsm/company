﻿#include "stdafx.h"
#include "CppUnitTest.h"
#include "Action.h"
#include "CompanyException.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CompanyTest
{
	// Тесты карты типов действий
	TEST_CLASS(ActionMapTest)
	{
	public:
		// Преобразование строки в тип
		TEST_METHOD(StringToType)
		{
			auto type = map.StringToType(L"убрать рабочее место");
			Assert::IsTrue(ActionType::Clean == type);
		}

		// Строка отсутствует в карте
		TEST_METHOD(NoString)
		{
			Assert::ExpectException<UnknownActionException>(
				std::bind(&ActionMap::StringToType, map, L"no string"));
		}

	private:
		// Карта, отражающая строку в тип действия
		ActionMap map;
	};
}