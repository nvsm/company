﻿#include "stdafx.h"
#include "CppUnitTest.h"
#include "TestingSpecialist.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CompanyTest
{
	// Тесты специалиста тестирования
	TEST_CLASS(TestingSpecialistTest)
	{
	public:
		TestingSpecialistTest() : testingSpecialist(L"Тест") {}

		// Тестировать
		TEST_METHOD(Test)
		{
			Assert::IsTrue(testingSpecialist.Execute(ActionType::Test));
		}

		// Составлять тестовые планы
		TEST_METHOD(MakeTestPlans)
		{
			Assert::IsTrue(testingSpecialist.Execute(ActionType::MakeTestPlans));
		}

		// Неподдерживаемое действие
		TEST_METHOD(UnsupportedAction)
		{
			Assert::IsFalse(testingSpecialist.Execute(ActionType::PaySalary));
		}

	private:
		// Специалист тестирования
		TestingSpecialist testingSpecialist;
	};
}