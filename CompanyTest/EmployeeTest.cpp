﻿#include "stdafx.h"
#include "CppUnitTest.h"
#include "Employee.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CompanyTest
{
	// Тесты сотрудника
	TEST_CLASS(EmployeeTest)
	{
	public:
		EmployeeTest() : employee(L"Тест") {}

		// Описание
		TEST_METHOD(Description)
		{
			Assert::AreEqual(L"сотрудник \"Тест\"", 
				employee.Description().c_str());
		}

		// Уйти в отпуск
		TEST_METHOD(Vacation)
		{
			Assert::IsTrue(employee.Execute(ActionType::Vacation));
		}

		// Убрать рабочее место
		TEST_METHOD(Clean)
		{
			Assert::IsTrue(employee.Execute(ActionType::Clean));
		}

		// Неподдерживаемое действие
		TEST_METHOD(UnsupportedAction)
		{
			Assert::IsFalse(employee.Execute(ActionType::Design));
		}

	private:
		// Сотрудник
		Employee employee;
	};
}